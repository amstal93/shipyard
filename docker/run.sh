#!/bin/bash

if [ -z "${1}" ]; then
    echo "No secret key for application provided!"
    exit 1
else
    /app/bin/shipyard -Dplay.http.secret.key="${1}"
fi
