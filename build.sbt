import Dependencies._

ThisBuild / version := "1.0-SNAPSHOT"
ThisBuild / organization := "org.swissbib"
ThisBuild / organizationName := "swissbib"

scalaVersion := "2.13.1"
maintainer := "sebastian.schuepbach@unibas.ch"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := """shipyard""",
    libraryDependencies ++= Seq(
      dockerJava,
      guice,
      jwtPlay,
      scalaTest % Test,
      scalaTestPlus % Test
    )
  )

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "org.swissbib.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "org.swissbib.binders._"
