/*
 * Copyright (C) 2019  Sebastian Schüpbach <sebastian.schuepbach@unibas.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import sbt._

object Dependencies {
  lazy val dockerJava = "com.github.docker-java" % "docker-java" % "3.1.5"
  lazy val jwtPlay = "com.pauldijou" %% "jwt-play" % "4.2.0"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.8"
  lazy val scalaTestPlus = "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3"

}
