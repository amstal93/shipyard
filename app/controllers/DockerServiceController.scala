/*
 * Copyright (C) 2019  Sebastian Schüpbach <sebastian.schuepbach@unibas.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package controllers

import javax.inject.Inject
import models.User
import play.api.mvc._
import utils.{DockerServices, JWTParseFailure, JWTParseSuccess, JwtServices}

import scala.util.{Failure, Success, Try}

class DockerServiceController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  private val logger = play.api.Logger(this.getClass)

  def createService(imageName: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    JwtServices.getCurrentUser(request) match {
      case JWTParseSuccess(user: User) =>
        Try(DockerServices.createService(imageName, user.id)) match {
          case Success(value) => Ok(value)
          case Failure(exception) =>
            logger.logger.error("An error occurred", exception)
            BadRequest(exception.getMessage)
        }
      case JWTParseSuccess(_) =>
        logger.logger.error(s"User doesn't match the model!")
        InternalServerError("Can't parse user")
      case f: JWTParseFailure =>
        logger.logger.warn(f.ex)
        BadRequest(f.ex)
    }
  }

  def removeService(id: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Try(DockerServices.removeService(id)) match {
      case Success(_) => Ok("removed")
      case Failure(exception) =>
        logger.logger.error("An error occurred", exception)
        BadRequest(exception.getMessage)
    }
  }

}
