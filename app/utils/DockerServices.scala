/*
 * Copyright (C) 2019  Sebastian Schüpbach <sebastian.schuepbach@unibas.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package utils

import java.util

import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.api.command._
import com.github.dockerjava.api.model._
import com.github.dockerjava.core.{DefaultDockerClientConfig, DockerClientBuilder, DockerClientConfig}
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import play.api.libs.json.{JsArray, JsObject, JsString, JsValue, Json}

import scala.collection.mutable
import scala.io.Source
import scala.jdk.CollectionConverters._

object DockerServices {

  private val logger = play.api.Logger(this.getClass)

  val settings: Config = ConfigFactory.defaultApplication()

  def getServices(user: String): mutable.Buffer[Service] =
    runDockerCmd[util.List[Service], ListServicesCmd](_.listServicesCmd()).asScala
      .filter(s => filterForUser(s.getSpec.getLabels, user))

  def serviceIsRunning(user: String, imageId: String): Boolean =
    getServices(user).map(s => s.getSpec.getLabels.get("shipyard.service.fromImage")).contains(imageId)

  def getMetadata(imageId: String): Option[Config] =
    settings.getConfigList("docker.images").asScala.find(md => md.getString("imageId") == imageId)

  def createService(imageName: String, user: String): String = {
    val serviceName = s"$user-${imageName.replaceAll("/", "-")}-${createRandomHash(3)}"
    val host = s"${createRandomHash(20)}.${settings.getString("docker.userServicesHost")}"
    val containerSpec = new ContainerSpec()
      .withImage(imageName)
    val taskSpec = new TaskSpec()
      .withContainerSpec(containerSpec)
    // TODO: Use configuration settings for keys
    val labels = Map(
      "shipyard.service.proprietor" -> user,
      "shipyard.service.url" -> host,
      "shipyard.service.fromImage" -> imageName,
      s"traefik.http.routers.$serviceName.rule" -> s"Host(`$host`)",
      s"traefik.http.services.$serviceName.loadbalancer.server.port" -> "8888",
      s"traefik.http.routers.$serviceName.tls" -> "true"
    ).asJava
    val network = new NetworkAttachmentConfig()
      .withTarget(settings.getString("docker.userServicesNetwork"))
    val serviceSpec = new ServiceSpec()
      .withTaskTemplate(taskSpec)
      .withName(serviceName)
      .withLabels(labels)
      .withNetworks(Seq(network).asJava)
    runDockerCmd[CreateServiceResponse, CreateServiceCmd](_.createServiceCmd(serviceSpec)).getId
  }

  def removeService(serviceId: String): Unit = {
    runDockerCmd[Void, RemoveServiceCmd](_.removeServiceCmd(serviceId))
  }

  def getDockerImages: List[String] = {
    val endpoint = s"${normalisePath(ConfigFactory.defaultApplication().getString("registry.url"))}_catalog"
    val objects = getRestContent(endpoint)
    objects match {
      case jsObj: JsObject if jsObj.keys.nonEmpty => jsObj.value.getOrElse("repositories", List()) match {
        case jsArr: JsArray => jsArr.value.toList.flatMap({
          case y: JsString => Some(y.value)
          case _ => None
        })
        case _ => List()
      }
      case _ => List()
    }

  }

  private val config: DockerClientConfig = DefaultDockerClientConfig.createDefaultConfigBuilder()
    .withRegistryUrl(settings.getString("registry.url"))
    .build()

  /**
   * Wrapper function for Docker commands. Cares about explicitly
   * closing resources to avoid leaks.
   *
   * @param f Docker command
   * @tparam B Final output
   * @tparam A Intermediary Docker command object
   * @return B
   */
  private def runDockerCmd[B, A <: SyncDockerCmd[B]](f: DockerClient => A): B = {
    val dockerClient = DockerClientBuilder.getInstance(config).build()
    val cmd = f(dockerClient)
    try
      cmd.exec()
    finally {
      cmd.close()
      dockerClient.close()
    }
  }

  private def getRestContent(url: String): JsValue = {
    val client = HttpClients.createDefault()
    val httpGet = new HttpGet(url)
    val response = client.execute(httpGet)
    val inputStream = response.getEntity.getContent
    try {
      Json.parse(Source.fromInputStream(inputStream).getLines().reduce(_ + _))
    } finally {
      inputStream.close()
      response.close()
      client.close()
    }
  }

  private def normalisePath(path: String): String =
    if (path.endsWith("/")) path else path + "/"

  private def filterForUser(labels: util.Map[String, String], user: String): Boolean = {
    labels.asScala.get("shipyard.service.proprietor") match {
      case Some(p) => p == user
      case _ => false
    }
  }

  private def createRandomHash(length: Int): String = {
    val r = scala.util.Random
    val reservoir = Range('a', 'z') ++ Range('0', '9')
    Range(0, length).map(_ => {
      val index = (r.nextInt % reservoir.size).abs
      reservoir(index).toChar
    }).mkString
  }

}
